# cmd

[![GoDoc](https://godoc.org/github.com/zro/cmd?status.svg)](https://godoc.org/github.com/zro/cmd)

Cmd gives us command/flag handling similar to that of the Go tools themselves.  This is
no more than a thin layer on top of `flag` from the standard library.

## Install

for Installation and usage of this library run the following.

>  `go get github.com/zro/cmd`

## Usage

The simplest way to see the usage of the library is to look at `example_test.go`
